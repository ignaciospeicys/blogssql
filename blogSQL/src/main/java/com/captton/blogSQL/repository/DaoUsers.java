package com.captton.blogSQL.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blogSQL.model.Users;

public interface DaoUsers extends JpaRepository<Users, Long> {

}
