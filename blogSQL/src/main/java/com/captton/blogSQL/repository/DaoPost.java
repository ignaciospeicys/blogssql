package com.captton.blogSQL.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blogSQL.model.Post;

public interface DaoPost extends JpaRepository<Post, Long> {

}
