package com.captton.blogSQL.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.blogSQL.model.Post;
import com.captton.blogSQL.model.PostComment;

public interface DaoPostComment extends JpaRepository<PostComment, Long> {

	public List<PostComment> findByPost(Post post);
}
