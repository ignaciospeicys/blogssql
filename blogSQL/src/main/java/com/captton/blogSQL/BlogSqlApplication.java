package com.captton.blogSQL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogSqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogSqlApplication.class, args);
	}

}
