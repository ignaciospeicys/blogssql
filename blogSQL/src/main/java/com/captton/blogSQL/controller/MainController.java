package com.captton.blogSQL.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.blogSQL.model.InputComment;
import com.captton.blogSQL.model.InputPostPost;
import com.captton.blogSQL.model.Post;
import com.captton.blogSQL.model.PostComment;
import com.captton.blogSQL.model.Users;
import com.captton.blogSQL.repository.DaoPost;
import com.captton.blogSQL.repository.DaoPostComment;
import com.captton.blogSQL.repository.DaoUsers;

@RestController
@RequestMapping({"/social"})
public class MainController {
	
	@Autowired
	private DaoUsers daousers;
	
	@Autowired
	private DaoPost daopost;
	
	@Autowired
	private DaoPostComment daocomment;
	
	@PostMapping(path = {"/users"})
	public Users create(@RequestBody Users user) {
		
		return daousers.save(user);
		
	}
	
	@PostMapping(path = {"/post"})
	public ResponseEntity<Object> createPost(@RequestBody InputPostPost post) {
		
		Users us1 = daousers.findById(post.getIdUser()).orElse(null);
		Post pos = new Post();
		
		if(us1!=null) {
			
			pos.setUser(us1);
			pos.setComment(post.getPost().getComment());
			pos.setTitle(post.getPost().getTitle());
			
			Post posteo = daopost.save(pos);
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("message", posteo.getId());
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("message", "USER NOT FOUND");
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}
	
	@PostMapping(path = {"/comment"})
	public ResponseEntity<Object> createComment(@RequestBody InputComment comment) {
		
		Users us1 = daousers.findById(comment.getIdUser()).orElse(null);
		Post pos1 = daopost.findById(comment.getIdPost()).orElse(null);
		
		if(us1!=null && pos1!= null) {
			
			PostComment comm = new PostComment();
			
			comm.setUser(us1);
			comm.setPost(pos1);
			comm.setComment(comment.getComment());
			
		PostComment commentResult =	daocomment.save(comm);
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 0);
			obj.put("message", commentResult.getId());
			
			return ResponseEntity.ok().body(obj.toString());
			
		} else {
			
			JSONObject obj = new JSONObject();
			
			obj.put("error", 1);
			obj.put("message", "USER or POST NOT FOUND");
			
			return ResponseEntity.ok().body(obj.toString());
			
		}
	}
	
	@GetMapping(path= {"/users"})
	public ResponseEntity<Object> getUsers() {
		
		List<Users> usersFound = daousers.findAll();
		
		JSONArray json_array = new JSONArray();
		
		if(usersFound.size()>0) {
			
			
			for(Users us : usersFound) {
				
				JSONObject aux = new JSONObject();
				aux.put("nombre", us.getNombre());
				aux.put("fechanacimiento", us.getFechanac());
				
				json_array.put(aux); //agrego el objeto json al array de jsons
				
			}
			
		} 
		
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("results", json_array);
		
		return ResponseEntity.ok().body(obj.toString());
		
	}
	
	@GetMapping(path = {"/comment/{idpost}"})
	public ResponseEntity<Object> getCommentsFromPost(@PathVariable Long idpost) {
		
		Post pos1 = daopost.findById(idpost).orElse(null);
		
		List<PostComment> commentsFound = daocomment.findByPost(pos1);
		
		JSONArray json_array = new JSONArray();
		
		if(commentsFound.size() > 0) {
			
			for(PostComment com : commentsFound) {
				
				JSONObject aux = new JSONObject();
				aux.put("comment", com.getComment());
				aux.put("Usuario", com.getUser().getNombre());
			
				json_array.put(aux);
				
			}
		}
		
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("results", json_array);
		
		
		return ResponseEntity.ok().body(obj.toString());
		
		
	}
	

}
